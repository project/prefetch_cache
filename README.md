Order of prefetching links (that is, sending ajax request to the server) can be defined with *prefetch-cache-weight* attribute:

    <a class="prefetch-link" prefetch-cache-weight="2" href='/some/path'>Link text</a>
Links are fetched in ascending weights order, with 0 as default. That is, links with negative weights are fetched first,then links without (valid numeric) *prefetch-cache-weight* attribute, and links with positive weights are fetched last.
