/**
 * @file
 * Processes links with the prefetch class.
 */

(function ($, Drupal) {

  "use strict";

  /**
   * Process links.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.prefetch_cache = {
    attach: function (context, settings) {
      $('a.prefetch-cache').sort(function (a,b) {
        var a_weight = isNaN(+ $(a).attr('prefetch-cache-weight')) ? 0 : + $(a).attr('prefetch-cache-weight');
        var b_weight = isNaN(+ $(b).attr('prefetch-cache-weight')) ? 0 : + $(b).attr('prefetch-cache-weight');
        if (a_weight == b_weight) {
          return 0;
        }
        else {
          return a_weight > b_weight ? 1 : -1;
        }
      }).
      once('prefetch-link').each(function () {

        var link = $(this);
        var href = link.attr('href');

        if (href.length > 0) {
          href = prepare_href_query(href);
          href += 'prefetch_cache_request=1';

          $.ajax({
            url: href
          })
            .done(function (response) {
              response = $.parseJSON(response);

              if (response.hasOwnProperty('prefetch_cache_token_id')) {
                href = link.attr('href');
                href = prepare_href_query(href);
                href += 'prefetch_cache_token_id=' + response.prefetch_cache_token_id;

                link.attr('href', href);
              }
            });
        }

      });

      function prepare_href_query(href) {
        if (href.indexOf('?') === -1) {
          href += '?';
        }
        else {
          href += '&';
        }
        return href;
      }
    }
  };

})(jQuery, Drupal);
